# Dr.Shevek.com

Repo for running my little hobby site, with various apps. Where necessary all docker images refer to the Raspberry Pi CPU architecture.

## Running

```bash
docker-compose up -d
```

## Troubleshooting

```bash
# webcam failing to display images
# test using fswebcam
fswebcam test-image.jpg
```

### Sources & References
* [linuxserver.io: LetsEncrypt + Nginx](https://blog.linuxserver.io/2019/04/25/letsencrypt-nginx-starter-guide/#containersetupexamples)
* [MotionEye](https://github.com/ccrisan/motioneye)
  * [Install in Docker](https://github.com/ccrisan/motioneye/wiki/Install-In-Docker)
  * [Running behind Nginx](https://github.com/ccrisan/motioneye/wiki/Running-Behind-Nginx)
* [Bookstack](https://hub.docker.com/r/linuxserver/bookstack)

### Other Potential Stuff
* [nextcloud](https://hub.docker.com/r/linuxserver/nextcloud)
  * open source cloud storage
* [openvpn](https://hub.docker.com/r/linuxserver/openvpn-as)
  * open source vpn?
* [wikijs](https://hub.docker.com/r/linuxserver/wikijs)
  * simple wiki editor and whatnot, better than bookstack?
* [davos](https://hub.docker.com/r/linuxserver/davos)
  * FTP
* [airsonic](https://hub.docker.com/r/linuxserver/airsonic)
  * Music vault/streamer/sharer
* [jellyfin](https://hub.docker.com/r/linuxserver/jellyfin)
  * opensource alternative to plex or emby
* [lychee](https://hub.docker.com/r/linuxserver/jellyfin)
  * photo management